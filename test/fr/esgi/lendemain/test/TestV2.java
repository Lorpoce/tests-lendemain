package fr.esgi.lendemain.test;

import org.junit.Assert;
import org.junit.Test;

import fr.esgi.tests.lendemain.model.Date;
import fr.esgi.tests.lendemain.utils.DateUtilsV2;

/**
 * Tests V2
 * 
 * (gestion du lendemain)
 */
public class TestV2 {

	@Test
	public void test() {

		Assert.assertEquals(true, new Date(21, 9, 2004).equals(DateUtilsV2.lendemain(20, 9, 2004)));

		Assert.assertEquals(true, new Date(1, 12, 2015).equals(DateUtilsV2.lendemain(30, 11, 2015)));

		Assert.assertEquals(true, new Date(30, 1, 2015).equals(DateUtilsV2.lendemain(29, 1, 2015)));

		Assert.assertEquals(true, new Date(30, 1, 2015).equals(DateUtilsV2.lendemain(29, 1, 2015)));

		Assert.assertEquals(true, new Date(1, 3, 2012).equals(DateUtilsV2.lendemain(29, 2, 2012)));

		// Bonne ann�e !
		Assert.assertEquals(true, new Date(1, 1, 2016).equals(DateUtilsV2.lendemain(31, 12, 2015)));
	}
}

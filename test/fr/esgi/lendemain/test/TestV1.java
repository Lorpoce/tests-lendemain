package fr.esgi.lendemain.test;

import org.junit.Test;

import fr.esgi.tests.lendemain.utils.DateUtilsV1;

/**
 * Tests V1
 * 
 * (validation des param�tres)
 */
public class TestV1 {

	/**
	 * Jour invalide
	 */
	@Test(expected = RuntimeException.class)
	public void testJourInvalide1() {

		DateUtilsV1.lendemain(300, 10, 1994);
	}

	/**
	 * Jour invalide
	 */
	@Test(expected = RuntimeException.class)
	public void testJourInvalide2() {

		DateUtilsV1.lendemain(35, 5, 1978);
	}

	/**
	 * Mois invalide
	 */
	@Test(expected = RuntimeException.class)
	public void testMoisInvalide1() {

		DateUtilsV1.lendemain(14, -300, 1914);
	}

	/**
	 * Mois invalide
	 */
	@Test(expected = RuntimeException.class)
	public void testMoisInvalide2() {

		DateUtilsV1.lendemain(23, 42, 1857);
	}

	/**
	 * Ann�e invalide
	 */
	@Test(expected = RuntimeException.class)
	public void testAnneeInvalide1() {

		DateUtilsV1.lendemain(8, 10, 0);
	}

	/**
	 * Ann�e invalide
	 */
	@Test(expected = RuntimeException.class)
	public void testAnneeInvalide2() {

		DateUtilsV1.lendemain(13, 7, 5000);
	}

	/**
	 * F�vrier non bissextile
	 */
	@Test(expected = RuntimeException.class)
	public void testFevrierNonBissextile1() {

		DateUtilsV1.lendemain(29, 2, 2015);
	}

	/**
	 * F�vrier non bissextile
	 */
	@Test(expected = RuntimeException.class)
	public void testFevrierNonBissextile2() {

		DateUtilsV1.lendemain(29, 2, 2200);
	}

}

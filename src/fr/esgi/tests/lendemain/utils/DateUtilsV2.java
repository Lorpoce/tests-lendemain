package fr.esgi.tests.lendemain.utils;

import java.util.Arrays;
import java.util.List;

import fr.esgi.tests.lendemain.model.Date;

/**
 * Utilitaires pour les {@link Date}
 * 
 * V2 (gestion du lendemain)
 */
public abstract class DateUtilsV2 {

	private static final int MIN_ANNEE = 1582;
	private static final int MAX_ANNEE = 3000;

	private static List<Integer> moisAvec31Jours = Arrays.asList(1, 3, 5, 7, 8, 10, 12);

	/**
	 * Lendemain
	 * 
	 * @param jour
	 * @param mois
	 * @param annee
	 * @return {@link Date}
	 */
	public static Date lendemain(int jour, int mois, int annee) {

		if (!isParametresValides(jour, mois, annee)) {
			throw new RuntimeException(
					"Param�tres invalides [Jour:" + jour + ";Mois:" + mois + ";Ann�e:" + annee + "]");
		}

		Date date = null;

		// Si c'est le dernier jour de l'ann�e
		if (isDernierJourAnnee(jour, mois)) {
			date = new Date(1, 1, annee + 1);
		}
		// Sinon si c'est le dernier jour du mois
		else if (isDernierJourMois(jour, mois, annee)) {
			date = new Date(1, mois + 1, annee);
		}
		// Sinon, c'est le jour suivant du mois
		else {
			date = new Date(jour + 1, mois, annee);
		}

		return date;
	}

	/**
	 * D�terminer s'il s'agit du dernier jour du mois
	 * 
	 * @param jour
	 * @param mois
	 * @param annee
	 * @return <code>true</code> si oui, <code>false</code> si non
	 */
	private static boolean isDernierJourMois(int jour, int mois, int annee) {

		if (mois == 2) {

			if (jour == 29) {

				return true;

			} else if (jour == 28) {

				return !isAnneeBissextile(annee);
			}
		} else if (jour == 30) {

			return !moisAvec31Jours.contains(mois);
		}

		return false;
	}

	/**
	 * D�terminer si c'est le dernier jour de l'ann�e
	 * 
	 * @param jour
	 * @param mois
	 * @return <code>true</code> si oui, <code>false</code> si non
	 */
	private static boolean isDernierJourAnnee(int jour, int mois) {
		return jour == 31 && mois == 12;
	}

	/**
	 * D�termine si les param�tres sont valides
	 * 
	 * @param jour
	 * @param mois
	 * @param annee
	 * @return <code>true</code> si valide, <code>false</code> si non
	 */
	private static boolean isParametresValides(int jour, int mois, int annee) {

		if (!isJourValide(jour) || !isMoisValide(mois) || !isAnneeValide(annee)
				|| ((mois == 2) && (jour == 29) && !isAnneeBissextile(annee))) {

			return false;

		} else {

			return true;
		}
	}

	/**
	 * D�termine si une ann�e esst bissextile
	 * 
	 * @param annee
	 * @return <code>true</code> si valide, <code>false</code> si non
	 */
	private static boolean isAnneeBissextile(int annee) {

		return ((annee % 4 == 00 && annee % 100 != 0) || (annee % 400 == 0));
	}

	/**
	 * D�termine si le jour est valide ou non
	 * 
	 * @param jour
	 * @return <code>true</code> si valide, <code>false</code> si non
	 */
	private static boolean isJourValide(int jour) {

		return ((jour >= 1) && (jour <= 31));
	}

	/**
	 * D�termine si l'ann�e est valide ou non
	 * 
	 * @param annee
	 * @return <code>true</code> si valide, <code>false</code> si non
	 */
	private static boolean isAnneeValide(int annee) {

		return ((annee > MIN_ANNEE) && (annee < MAX_ANNEE));
	}

	/**
	 * D�termine si le mois est valide ou non
	 * 
	 * @param mois
	 * @return <code>true</code> si valide, <code>false</code> si non
	 */
	private static boolean isMoisValide(int mois) {

		return ((mois >= 1) && (mois <= 12));
	}

}

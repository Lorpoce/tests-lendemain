package fr.esgi.tests.lendemain.utils;

import fr.esgi.tests.lendemain.model.Date;

/**
 * Utilitaires pour les {@link Date}
 * 
 * V1 (validation des param�tres)
 */
public abstract class DateUtilsV1 {

	private static final int MIN_ANNEE = 1582;
	private static final int MAX_ANNEE = 3000;

	/**
	 * Lendemain
	 * 
	 * @param jour
	 * @param mois
	 * @param annee
	 * @return {@link Date}
	 */
	public static Date lendemain(int jour, int mois, int annee) {

		if (!isParametresValides(jour, mois, annee)) {
			throw new RuntimeException("Param�tres invalides [Jour:" + jour + ";Mois:" + mois + ";Ann�e:" + annee);
		}

		return null;
	}

	/**
	 * D�termine si les param�tres sont valides
	 * 
	 * @param jour
	 * @param mois
	 * @param annee
	 * @return <code>true</code> si valide, <code>false</code> si non
	 */
	private static boolean isParametresValides(int jour, int mois, int annee) {

		if (!isJourValide(jour) || !isMoisValide(mois) || !isAnneeValide(annee)
				|| ((mois == 2) && (jour == 29) && !isAnneeBissextile(annee))) {

			return false;

		} else {

			return true;
		}
	}

	/**
	 * D�termine si une ann�e esst bissextile
	 * 
	 * @param annee
	 * @return
	 */
	private static boolean isAnneeBissextile(int annee) {

		return ((annee % 4 == 00 && annee % 100 != 0) || (annee % 400 == 0));
	}

	/**
	 * D�termine si le jour est valide ou non
	 * 
	 * @param jour
	 * @return <code>true</code> si valide, <code>false</code> si non
	 */
	private static boolean isJourValide(int jour) {

		return ((jour >= 1) && (jour <= 31));
	}

	/**
	 * D�termine si l'ann�e est valide ou non
	 * 
	 * @param annee
	 * @return <code>true</code> si valide, <code>false</code> si non
	 */
	private static boolean isAnneeValide(int annee) {

		return ((annee > MIN_ANNEE) && (annee < MAX_ANNEE));
	}

	/**
	 * D�termine si le mois est valide ou non
	 * 
	 * @param mois
	 * @return <code>true</code> si valide, <code>false</code> si non
	 */
	private static boolean isMoisValide(int mois) {

		return ((mois >= 1) && (mois <= 12));
	}

}

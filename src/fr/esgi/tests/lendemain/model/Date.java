package fr.esgi.tests.lendemain.model;

import lombok.Data;

/**
 * Représente une date
 */
@Data
public class Date {

	private int annee;
	private int mois;
	private int jour;

	public Date(int jour, int mois, int annee) {
		this.jour = jour;
		this.mois = mois;
		this.annee = annee;
	}

}
